package com.example.mypc.chat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity{

    private Socket mSocket;
    {
        try {
            mSocket= IO.socket("http://192.168.220.1:3000");
        }catch (URISyntaxException e){}
    }

    private CircleImageView imglogo;
    private TextView txtnameapp;
    private ImageView imgaccount,imgpass;
    private EditText edtaccount,edtpass;
    private Button btnLogin,btnregister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findID();
        Onclick();
        mSocket.connect();
    }
    private void findID(){
        imglogo= findViewById(R.id.imglogo);
        txtnameapp= findViewById(R.id.txtnameapp);
        imgaccount= findViewById(R.id.imgaccount);
        imgpass= findViewById(R.id.imgpass);
        edtaccount= findViewById(R.id.edtaccount);
        edtpass= findViewById(R.id.edtpass);
        btnLogin= findViewById(R.id.btnLogin);
        btnregister= findViewById(R.id.btnregister);
    }
    private void Onclick(){
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(MainActivity.this, ChatActivity.class);
                startActivity(intent);
            }
        });
        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }
}
