package com.example.mypc.chat;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener{

    private static final int PICK_IMAGE = 222;

    private ListView lvChat;
    private EditText edtnhapND;
    private ImageView imgSendImg;
    private  ImageView imgSend;
    private ArrayList<String> arrchat, arrimage;
    private Dialog dialogImage;
    public Socket mSocket;
    {
        try {
            mSocket= IO.socket("http://192.168.220.1:3000");
        }catch (URISyntaxException e){}
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        findID();
        arrchat= new ArrayList<String>();
        mSocket.on("server-gui-tinnhan",onNewMessage_DanhsachtinChat);
        //arrimage= new ArrayList<String>();
        //mSocket.on("server-gui-hinhanh",onSendImagine);
        imgSend.setOnClickListener(this);
        imgSendImg.setOnClickListener(this);

    }
    private void findID(){
        lvChat= findViewById(R.id.lvChat);
        edtnhapND= findViewById(R.id.edtnhapND);
        imgSendImg= findViewById(R.id.imgSendImg);
        imgSend= findViewById(R.id.imgSend);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgSend:
                mSocket.emit("client-gui-tinnhan",edtnhapND.getText().toString());
                edtnhapND.getText().clear();
                break;
            case R.id.imgSendImg:
                initDialogImage();
                //mSocket.emit("client-gui-hinhanh",edtnhapND.getText().toString());
                break;
        }
    }
//    private Emitter.Listener onSendImagine= new Emitter.Listener() {
//        @Override
//        public void call(final Object... args) {
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    imgSendImg.setImageBitmap(getBitmapFromBase64(args[0].toString()));
//                }
//
//                public Bitmap getBitmapFromBase64(String string) {
//                    byte[] bitmapArray = null;
//                    try {
//                        bitmapArray = Base64.decode(string, Base64.DEFAULT);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    return BitmapFactory.decodeByteArray(bitmapArray, 0,
//                            bitmapArray.length);
//                };
//            });
//        }
//    };
//
    private void initDialogImage(){
        dialogImage= new Dialog(ChatActivity.this,android.R.style.Theme_Holo_Dialog_NoActionBar_MinWidth);
        dialogImage.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogImage.setContentView(R.layout.dialog_image_chose);
        TextView txtchoseimage= dialogImage.findViewById(R.id.txtchoseimage);
        TextView txtchosefile= dialogImage.findViewById(R.id.txtchosefile);
        ImageView imgaddimage= dialogImage.findViewById(R.id.imgaddimage);
        txtchosefile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //dialogImage.show();
                Intent intent1= new Intent();
                intent1.setType("image/*");
                intent1.setAction(Intent.ACTION_GET_CONTENT);
                Intent pickIntent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");
                Intent chooserIntent = Intent.createChooser(getIntent(),
                        getString(R.string.supervisor_profile_choose_image_title));
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});
                startActivityForResult(chooserIntent,PICK_IMAGE);
            }
        });

    }
    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {

            try {
                Uri imageUri = data.getData();
                Bitmap photo = MediaStore.Images.Media.getBitmap(this.getContentResolver(),
                        imageUri);
            } catch (IOException e) {

            }

            return;
        }
    }

   private Emitter.Listener onNewMessage_DanhsachtinChat= new Emitter.Listener() {
       @Override
       public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data= (JSONObject) args[0];
                    String noidung;
                    try {
                        noidung= data.getString("tinchat");
                        arrchat.add(noidung);
                        ArrayAdapter adapter1= new ArrayAdapter(ChatActivity.this,android.R.layout.simple_list_item_1,arrchat);
                        lvChat.setAdapter(adapter1);
                    } catch (JSONException e) {
                        return;
                    }
                }
            });
       }
   };
}
