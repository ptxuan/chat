package com.example.mypc.chat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

public class RegisterActivity extends AppCompatActivity {

    private Socket mSocket;
    {
        try {
            mSocket= IO.socket("http://192.168.220.1:3000");
        }catch (URISyntaxException e){}
    }

    private ImageView imgacname,imgemail,imgnhappass;
    private EditText edtnhapten,edtnhapemail,edtnhappass;
    private Button btndangki;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        findID();
        click();
        mSocket.connect();
        mSocket.on("ketquaDangKyUserName",OnNewMessage_Register);
        mSocket.on("server-gui-username",OnNewMessage_ListRegister);
    }
    private void findID(){
        imgacname= findViewById(R.id.imgacname);
        imgemail= findViewById(R.id.imgemail);
        imgnhappass= findViewById(R.id.imgnhappass);
        edtnhapten= findViewById(R.id.edtnhapten);
        edtnhapemail= findViewById(R.id.edtnhapemail);
        edtnhappass= findViewById(R.id.edtnhappass);
        btndangki= findViewById(R.id.btndangki);
    }
    private void click(){
        btndangki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSocket.emit("client-gui-username",edtnhapten.getText().toString());
                //mSocket.emit("client-gui-username",edtnhappass.getText().toString());
                Intent intent= new Intent(RegisterActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
    private Emitter.Listener OnNewMessage_Register= new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String noidung;

                    try {
                        noidung = data.getString("noidung");
                        if (noidung=="true"){
                            Toast.makeText(RegisterActivity.this, "Dang ki thanh cong", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(RegisterActivity.this, "Dang ky that bai", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        return;
                    }

                }
            });
        }
    };
    private Emitter.Listener OnNewMessage_ListRegister= new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    JSONArray noidung;

                    try {
                        noidung = data.getJSONArray("danhsach");
                        Toast.makeText(RegisterActivity.this, noidung.length()+" ", Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        return;
                    }

                }
            });
        }
    };
}
